package Modelo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class SorteoCarton {
	
	private Random aleatorio;
	private ArrayList<Integer> listaNumeros;
	
	public void CrearListaAleatoria (ArrayList<Integer> listaNumeros) {
		Integer numeroAleatorio;
		listaNumeros.clear();
		for (int i = 0; i < 6; i++) {
			numeroAleatorio = aleatorio.nextInt(50);
			listaNumeros.add(numeroAleatorio);
		}
	}
	
	public boolean ValorRepetido (ArrayList<Integer> listaNumeros) {
		Boolean logico = new Boolean(true);
		ArrayList<Integer> listaComparar = new ArrayList<Integer>(listaNumeros);
		for (Integer valor1 : listaNumeros) {
			for (Integer valor2 : listaComparar) {
				if (valor1.equals(valor2)) {
					logico = false;
				}
			}
		}
		return logico;
	}
	
	public void OrdenarLista (ArrayList<Integer> listaNumeros) {
		Collections.sort(listaNumeros);
	}
	
	public SorteoCarton(Random aleatorio, ArrayList<Integer> listaNumeros) {
		super();
		this.aleatorio = aleatorio;
		this.listaNumeros = listaNumeros;
	}

	public Random getAleatorio() {
		return aleatorio;
	}
	public void setAleatorio(Random aleatorio) {
		this.aleatorio = aleatorio;
	}
	public ArrayList<Integer> getListaNumeros() {
		return listaNumeros;
	}
	public void setListaNumeros(ArrayList<Integer> listaNumeros) {
		this.listaNumeros = listaNumeros;
	}

	@Override
	public String toString() {
		return "SorteoCarton: " + "/n" + "Aleatorio: " + aleatorio + ", listaNumeros: " + listaNumeros;
	}
	
	
	
}
