package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JButton;

import Modelo.SorteoCarton;
import Vista.VistaCarton;

public class ControladorCarton implements ActionListener {
	
	private SorteoCarton modelo;
	private VistaCarton vista;

	public SorteoCarton getModelo() {
		return modelo;
	}

	public void setModelo(SorteoCarton modelo) {
		this.modelo = modelo;
	}

	public VistaCarton getVista() {
		return vista;
	}

	public void setVista(VistaCarton vista) {
		this.vista = vista;
	}
	
	public ControladorCarton() {
		super();
		this.setModelo(modelo);
		this.setVista(new VistaCarton(this));
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		JButton btn = (JButton) arg0.getSource();
		if (btn.getText().equals("Jugar")) {
			String Valor1 = this.getVista().getTextValor1().getText();
			String Valor2 = this.getVista().getTextValor2().getText();
			String Valor3 = this.getVista().getTextValor3().getText();
			String Valor4 = this.getVista().getTextValor4().getText();
			String Valor5 = this.getVista().getTextValor5().getText();
			String Valor6 = this.getVista().getTextValor6().getText();
			ArrayList<Integer> listaNumeros = new ArrayList<Integer>();
			listaNumeros.add(Integer.parseInt(Valor1));
			listaNumeros.add(Integer.parseInt(Valor2));
			listaNumeros.add(Integer.parseInt(Valor3));
			listaNumeros.add(Integer.parseInt(Valor4));
			listaNumeros.add(Integer.parseInt(Valor5));
			listaNumeros.add(Integer.parseInt(Valor6));
			//Collections.sort(listaNumeros);
			modelo.OrdenarLista(listaNumeros);
			this.getVista().getNumSelec1().setText(String.valueOf(listaNumeros.get(0)));
			this.getVista().getNumSelec2().setText(String.valueOf(listaNumeros.get(1)));
			this.getVista().getNumSelec3().setText(String.valueOf(listaNumeros.get(2)));
			this.getVista().getNumSelec4().setText(String.valueOf(listaNumeros.get(3)));
			this.getVista().getNumSelec5().setText(String.valueOf(listaNumeros.get(4)));
			this.getVista().getNumSelec6().setText(String.valueOf(listaNumeros.get(5)));
			ArrayList<Integer> listaAleatoria = new ArrayList<Integer>();
			modelo.CrearListaAleatoria(listaAleatoria);
			//listaNumeros = modelo.OrdenarLista(listaAleatoria);
		}
	}

}
