package Vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Controlador.ControladorCarton;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;

public class VistaCarton extends JFrame {
	
	private ControladorCarton controlador;
	private JPanel contentPane;
	private JTextField textValor1;
	private JTextField textValor2;
	private JTextField textValor3;
	private JTextField textValor4;
	private JTextField textValor5;
	private JTextField textValor6;
	private JLabel numSelec1;
	private JLabel numSelec2;
	private JLabel numSelec3;
	private JLabel numSelec4;
	private JLabel numSelec5;
	private JLabel numSelec6;
	private JLabel numSort1;
	private JLabel numSort2;
	private JLabel numSort3;
	private JLabel numSort4;
	private JLabel numSort5;
	private JLabel numSort6;
	private JButton btnJugar;
	private JButton btnNuevaPartida;
	private JLabel lblMostrarResultado;

	/**
	 * Launch the application.
	 */
	/**
	 * Create the frame.
	 */
	public VistaCarton(ControladorCarton controladorCarton) {
		this.setControlador(controladorCarton);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 330);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCarton = new JLabel("Carton");
		lblCarton.setFont(new Font("Verdana", Font.BOLD, 18));
		lblCarton.setBounds(150, 5, 75, 30);
		contentPane.add(lblCarton);
		
		JLabel lblSeleccione = new JLabel("Seleccione 6 numeros");
		lblSeleccione.setFont(new Font("Verdana", Font.PLAIN, 13));
		lblSeleccione.setBounds(10, 35, 150, 25);
		contentPane.add(lblSeleccione);
		
		textValor1 = new JTextField();
		textValor1.setBounds(10, 70, 110, 25);
		contentPane.add(textValor1);
		textValor1.setColumns(10);
		
		textValor2 = new JTextField();
		textValor2.setColumns(10);
		textValor2.setBounds(135, 70, 110, 25);
		contentPane.add(textValor2);
		
		textValor3 = new JTextField();
		textValor3.setColumns(10);
		textValor3.setBounds(260, 70, 110, 25);
		contentPane.add(textValor3);
		
		textValor4 = new JTextField();
		textValor4.setColumns(10);
		textValor4.setBounds(10, 105, 110, 25);
		contentPane.add(textValor4);
		
		textValor5 = new JTextField();
		textValor5.setColumns(10);
		textValor5.setBounds(135, 105, 110, 25);
		contentPane.add(textValor5);
		
		textValor6 = new JTextField();
		textValor6.setColumns(10);
		textValor6.setBounds(260, 105, 110, 25);
		contentPane.add(textValor6);
		
		btnJugar = new JButton("Jugar");
		btnJugar.addActionListener(this.getControlador());
		btnJugar.setFont(new Font("Verdana", Font.PLAIN, 15));
		btnJugar.setBounds(135, 140, 110, 25);
		contentPane.add(btnJugar);
		
		JLabel lblNumerosSeleccionados = new JLabel("Numeros Seleccionados: ");
		lblNumerosSeleccionados.setFont(new Font("Verdana", Font.PLAIN, 13));
		lblNumerosSeleccionados.setBounds(10, 170, 170, 25);
		contentPane.add(lblNumerosSeleccionados);
		
		JLabel lblNumerosSorteados = new JLabel("Numeros Sorteados: ");
		lblNumerosSorteados.setFont(new Font("Verdana", Font.PLAIN, 13));
		lblNumerosSorteados.setBounds(10, 195, 170, 25);
		contentPane.add(lblNumerosSorteados);
		
		btnNuevaPartida = new JButton("Nueva Partida");
		btnNuevaPartida.setFont(new Font("Verdana", Font.PLAIN, 13));
		btnNuevaPartida.setBounds(125, 230, 130, 25);
		contentPane.add(btnNuevaPartida);
		
		numSelec1 = new JLabel("");
		numSelec1.setFont(new Font("Verdana", Font.BOLD, 13));
		numSelec1.setBounds(175, 170, 25, 25);
		contentPane.add(numSelec1);
		
		numSelec2 = new JLabel("");
		numSelec2.setFont(new Font("Verdana", Font.BOLD, 13));
		numSelec2.setBounds(200, 170, 25, 25);
		contentPane.add(numSelec2);
		
		numSelec3 = new JLabel("");
		numSelec3.setFont(new Font("Verdana", Font.BOLD, 13));
		numSelec3.setBounds(225, 170, 25, 25);
		contentPane.add(numSelec3);
		
		numSelec4 = new JLabel("");
		numSelec4.setFont(new Font("Verdana", Font.BOLD, 13));
		numSelec4.setBounds(250, 170, 25, 25);
		contentPane.add(numSelec4);
		
		numSelec5 = new JLabel("");
		numSelec5.setFont(new Font("Verdana", Font.BOLD, 13));
		numSelec5.setBounds(275, 170, 25, 25);
		contentPane.add(numSelec5);
		
		numSelec6 = new JLabel("");
		numSelec6.setFont(new Font("Verdana", Font.BOLD, 13));
		numSelec6.setBounds(300, 170, 25, 25);
		contentPane.add(numSelec6);
		
		numSort1 = new JLabel("");
		numSort1.setFont(new Font("Verdana", Font.BOLD, 13));
		numSort1.setBounds(175, 195, 25, 25);
		contentPane.add(numSort1);
		
		numSort2 = new JLabel("");
		numSort2.setFont(new Font("Verdana", Font.BOLD, 13));
		numSort2.setBounds(200, 195, 25, 25);
		contentPane.add(numSort2);
		
		numSort3 = new JLabel("");
		numSort3.setFont(new Font("Verdana", Font.BOLD, 13));
		numSort3.setBounds(225, 195, 25, 25);
		contentPane.add(numSort3);
		
		numSort4 = new JLabel("");
		numSort4.setFont(new Font("Verdana", Font.BOLD, 13));
		numSort4.setBounds(250, 195, 25, 25);
		contentPane.add(numSort4);
		
		numSort5 = new JLabel("");
		numSort5.setFont(new Font("Verdana", Font.BOLD, 13));
		numSort5.setBounds(275, 195, 25, 25);
		contentPane.add(numSort5);
		
		numSort6 = new JLabel("");
		numSort6.setFont(new Font("Verdana", Font.BOLD, 13));
		numSort6.setBounds(300, 195, 25, 25);
		contentPane.add(numSort6);
		
		lblMostrarResultado = new JLabel("");
		lblMostrarResultado.setFont(new Font("Verdana", Font.PLAIN, 13));
		lblMostrarResultado.setBounds(10, 265, 350, 25);
		contentPane.add(lblMostrarResultado);
	}

	public JTextField getTextValor1() {
		return textValor1;
	}



	public void setTextValor1(JTextField textValor1) {
		this.textValor1 = textValor1;
	}



	public JTextField getTextValor2() {
		return textValor2;
	}



	public void setTextValor2(JTextField textValor2) {
		this.textValor2 = textValor2;
	}



	public JTextField getTextValor3() {
		return textValor3;
	}



	public void setTextValor3(JTextField textValor3) {
		this.textValor3 = textValor3;
	}



	public JTextField getTextValor4() {
		return textValor4;
	}



	public void setTextValor4(JTextField textValor4) {
		this.textValor4 = textValor4;
	}



	public JTextField getTextValor5() {
		return textValor5;
	}



	public void setTextValor5(JTextField textValor5) {
		this.textValor5 = textValor5;
	}



	public JTextField getTextValor6() {
		return textValor6;
	}



	public void setTextValor6(JTextField textValor6) {
		this.textValor6 = textValor6;
	}



	public JLabel getNumSelec1() {
		return numSelec1;
	}



	public void setNumSelec1(JLabel numSelec1) {
		this.numSelec1 = numSelec1;
	}



	public JLabel getNumSelec2() {
		return numSelec2;
	}



	public void setNumSelec2(JLabel numSelec2) {
		this.numSelec2 = numSelec2;
	}



	public JLabel getNumSelec3() {
		return numSelec3;
	}



	public void setNumSelec3(JLabel numSelec3) {
		this.numSelec3 = numSelec3;
	}



	public JLabel getNumSelec4() {
		return numSelec4;
	}



	public void setNumSelec4(JLabel numSelec4) {
		this.numSelec4 = numSelec4;
	}



	public JLabel getNumSelec5() {
		return numSelec5;
	}



	public void setNumSelec5(JLabel numSelec5) {
		this.numSelec5 = numSelec5;
	}



	public JLabel getNumSelec6() {
		return numSelec6;
	}



	public void setNumSelec6(JLabel numSelec6) {
		this.numSelec6 = numSelec6;
	}



	public JLabel getNumSort1() {
		return numSort1;
	}



	public void setNumSort1(JLabel numSort1) {
		this.numSort1 = numSort1;
	}



	public JLabel getNumSort2() {
		return numSort2;
	}



	public void setNumSort2(JLabel numSort2) {
		this.numSort2 = numSort2;
	}



	public JLabel getNumSort3() {
		return numSort3;
	}



	public void setNumSort3(JLabel numSort3) {
		this.numSort3 = numSort3;
	}



	public JLabel getNumSort4() {
		return numSort4;
	}



	public void setNumSort4(JLabel numSort4) {
		this.numSort4 = numSort4;
	}



	public JLabel getNumSort5() {
		return numSort5;
	}



	public void setNumSort5(JLabel numSort5) {
		this.numSort5 = numSort5;
	}



	public JLabel getNumSort6() {
		return numSort6;
	}



	public void setNumSort6(JLabel numSort6) {
		this.numSort6 = numSort6;
	}



	public JButton getBtnJugar() {
		return btnJugar;
	}



	public void setBtnJugar(JButton btnJugar) {
		this.btnJugar = btnJugar;
	}



	public JButton getBtnNuevaPartida() {
		return btnNuevaPartida;
	}



	public void setBtnNuevaPartida(JButton btnNuevaPartida) {
		this.btnNuevaPartida = btnNuevaPartida;
	}



	public JLabel getLblMostrarResultado() {
		return lblMostrarResultado;
	}



	public void setLblMostrarResultado(JLabel lblMostrarResultado) {
		this.lblMostrarResultado = lblMostrarResultado;
	}



	public ControladorCarton getControlador() {
		return controlador;
	}

	public void setControlador(ControladorCarton controlador) {
		this.controlador = controlador;
	}
}
