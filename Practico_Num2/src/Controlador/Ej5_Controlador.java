package Controlador;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.swing.JButton;

import Modelo.Ej4_Banco;
import Modelo.Ej5_Modelo;
import Vista.Ej5_Vista;

public class Ej5_Controlador implements ActionListener {
	
	private Ej5_Vista vista;
	private Ej5_Modelo modelo;
	private Ej4_Banco banco;
	
	public Ej5_Controlador() {
		super();
		this.setBanco(banco);
		this.setVista(new Ej5_Vista(this));
	}
	
	public Ej5_Vista getVista() {
		return vista;
	}
	public void setVista(Ej5_Vista vista) {
		this.vista = vista;
	}
	public Ej5_Modelo getModelo() {
		return modelo;
	}
	public void setModelo(Ej5_Modelo modelo) {
		this.modelo = modelo;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		JButton btn = (JButton) arg0.getSource();
		if (btn.getText().equals("Guardar")) {
			String nombre = this.getVista().getTextField_Nombre().getText();
			String genero = this.getVista().getTextField_Genero().getText();
			String email = this.getVista().getTextField_Email().getText();
			String contraseņa = this.getVista().getTextField_Contraseņa().getText();
			LocalDate fechaNac = LocalDate.parse(this.getVista().getTextField_FechaNac().getText(),DateTimeFormatter.ofPattern("dd/MM/yyyy"));
			Modelo.Ej4_Persona persona = new Modelo.Ej4_Persona(nombre, genero, email, contraseņa, fechaNac);
			System.out.println("Se guardo correctamente");
			Ej4_Banco banco = new Ej4_Banco("Frances");
			banco.agregarCuenta(persona, 2000.0);
			banco.listarCliente();
		}
	}

	public Ej4_Banco getBanco() {
		return banco;
	}

	public void setBanco(Ej4_Banco banco) {
		this.banco = banco;
	}
	
}
