package Modelo;
import java.time.LocalDate;

public class Ej4_Persona {
	
	private String nombre;
	private String genero;
	private String email;
	private String contraseña;
	private LocalDate fecha_Nac;
	
	public Ej4_Persona(String nombre, String genero, String email, String contraseña, LocalDate fecha_Nac) {
		super();
		this.nombre = nombre;
		this.genero = genero;
		this.email = email;
		this.contraseña = contraseña;
		this.fecha_Nac = fecha_Nac;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContraseña() {
		return contraseña;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}

	public LocalDate getFecha_Nac() {
		return fecha_Nac;
	}

	public void setFecha_Nac(LocalDate fecha_Nac) {
		this.fecha_Nac = fecha_Nac;
	}

	@Override
	public String toString() {
		return "nombre=" + nombre + ", genero=" + genero + ", email=" + email + ", contraseña="
				+ contraseña + ", fecha_Nac=" + fecha_Nac;
	}
	
	
	
}
