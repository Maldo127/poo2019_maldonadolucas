package Modelo;
import java.util.HashMap;
public class Ej4_Banco {
	
	private String nombre_banco;
	private HashMap<Ej4_Persona,Double> cuenta;
	
	
	public Ej4_Banco(String nombre_banco) {
		super();
		this.nombre_banco = nombre_banco;
		this.cuenta = new HashMap<>();
	}
	public Boolean agregarCuenta(Ej4_Persona persona,Double saldo) {
		if (this.cuenta.equals(persona)) {
			return true;
		} else {
			this.cuenta.put(persona, saldo);
			return false;
		}
	}
	public Boolean eliminarCuenta(Ej4_Persona persona) {
		if (cuenta.containsKey(persona)) {
			cuenta.remove(persona);
			return true;
		} else {
			return false;
		}
	}
	public void listarCliente () {
		for (Ej4_Persona string : cuenta.keySet()) {
			System.out.println(string);
			}
	}
	public Double getSaldo (Ej4_Persona persona) {
		return this.cuenta.get(persona);
	}
	
	public Ej4_Banco(String nombre_banco, HashMap<Ej4_Persona, Double> cuenta) {
		super();
		this.nombre_banco = nombre_banco;
		this.cuenta = cuenta;
	}
	
	public String getNombre_banco() {
		return nombre_banco;
	}
	public void setNombre_banco(String nombre_banco) {
		this.nombre_banco = nombre_banco;
	}
}