package Vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Controlador.Ej5_Controlador;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class Ej5_Vista extends JFrame {
	
	private Ej5_Controlador controlador;
	private JPanel contentPane;
	private JTextField textField_Nombre;
	private JTextField textField_Genero;
	private JTextField textField_Email;
	private JTextField textField_Contraseņa;
	private JTextField textField_FechaNac;
	private JButton btnGuardar;
	/**
	 * Launch the application.
	 */
	
	/**
	 * Create the frame.
	 */
	public Ej5_Vista(Ej5_Controlador controlador) {
		this.setControlador(controlador);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 275);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener((this.getControlador()));
		btnGuardar.setFont(new Font("Verdana", Font.PLAIN, 15));
		btnGuardar.setBounds(147, 201, 113, 23);
		contentPane.add(btnGuardar);
		
		JLabel lblNewLabel = new JLabel("Nueva Cuenta");
		lblNewLabel.setFont(new Font("Verdana", Font.PLAIN, 15));
		lblNewLabel.setBounds(140, 5, 120, 30);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Nombre:");
		lblNewLabel_1.setFont(new Font("Verdana", Font.PLAIN, 12));
		lblNewLabel_1.setBounds(15, 40, 150, 30);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Genero: ");
		lblNewLabel_2.setFont(new Font("Verdana", Font.PLAIN, 12));
		lblNewLabel_2.setBounds(15, 70, 150, 30);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Email: ");
		lblNewLabel_3.setFont(new Font("Verdana", Font.PLAIN, 12));
		lblNewLabel_3.setBounds(15, 100, 150, 30);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Contrase\u00F1a: ");
		lblNewLabel_4.setFont(new Font("Verdana", Font.PLAIN, 12));
		lblNewLabel_4.setBounds(15, 130, 150, 30);
		contentPane.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Fecha de Nacimiento: ");
		lblNewLabel_5.setFont(new Font("Verdana", Font.PLAIN, 12));
		lblNewLabel_5.setBounds(15, 160, 150, 30);
		contentPane.add(lblNewLabel_5);
		
		JTextField textField_Nombre = new JTextField();
		textField_Nombre.setBounds(175, 40, 185, 25);
		contentPane.add(textField_Nombre);
		textField_Nombre.setColumns(10);
		
		JTextField textField_Genero = new JTextField();
		textField_Genero.setColumns(10);
		textField_Genero.setBounds(175, 70, 185, 25);
		contentPane.add(textField_Genero);
		
		JTextField textField_Email = new JTextField();
		textField_Email.setColumns(10);
		textField_Email.setBounds(175, 100, 185, 25);
		contentPane.add(textField_Email);
		
		JTextField textField_Contraseņa = new JTextField();
		textField_Contraseņa.setColumns(10);
		textField_Contraseņa.setBounds(175, 130, 185, 25);
		contentPane.add(textField_Contraseņa);
		
		JTextField textField_FechaNac = new JTextField();
		textField_FechaNac.setColumns(10);
		textField_FechaNac.setBounds(175, 160, 185, 25);
		contentPane.add(textField_FechaNac);
	}

	public Ej5_Controlador getControlador() {
		return controlador;
	}

	public void setControlador(Ej5_Controlador controlador) {
		this.controlador = controlador;
	}

	public JButton getBtnGuardar() {
		return btnGuardar;
	}

	public void setBtnGuardar(JButton btnGuardar) {
		this.btnGuardar = btnGuardar;
	}

	public JTextField getTextField_Nombre() {
		return textField_Nombre;
	}

	public void setTextField_Nombre(JTextField textField_Nombre) {
		this.textField_Nombre = textField_Nombre;
	}

	public JTextField getTextField_Genero() {
		return textField_Genero;
	}

	public void setTextField_Genero(JTextField textField_Genero) {
		this.textField_Genero = textField_Genero;
	}

	public JTextField getTextField_Contraseņa() {
		return textField_Contraseņa;
	}

	public void setTextField_Contraseņa(JTextField textField_Contraseņa) {
		this.textField_Contraseņa = textField_Contraseņa;
	}

	public JTextField getTextField_FechaNac() {
		return textField_FechaNac;
	}

	public void setTextField_FechaNac(JTextField textField_FechaNac) {
		this.textField_FechaNac = textField_FechaNac;
	}

	public JTextField getTextField_Email() {
		return textField_Email;
	}

	public void setTextField_Email(JTextField textField_Email) {
		this.textField_Email = textField_Email;
	}
}
